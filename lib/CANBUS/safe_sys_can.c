#include "safe_sys_can.h"


#ifdef EVT_USE_DIAG_MONITORS
// Function prototypes to be called each time CAN frame is unpacked
// FMon function may detect RC, CRC or DLC violation
#include "evt-fmon.h"

#endif // EVT_USE_DIAG_MONITORS


uint32_t Unpack_safety_system_evt(safety_system_t* _m, const uint8_t* _d, uint8_t dlc_)
{
  (void)dlc_;
  _m->throttle = ((_d[1] & (0xFFU)) << 8) | (_d[0] & (0xFFU));
  _m->steer = ((_d[3] & (0xFFU)) << 8) | (_d[2] & (0xFFU));
  _m->deadman = ((_d[4] >> 4) & (0x01U));
  _m->auto_mux = (_d[5] & (0x01U));
  _m->init = ((_d[5] >> 4) & (0x01U));
  _m->calib = (_d[6] & (0x01U));

#ifdef EVT_USE_DIAG_MONITORS
  _m->mon1.dlc_error = (dlc_ < safety_system_DLC);
  _m->mon1.last_cycle = GetSystemTick();
  _m->mon1.frame_cnt++;

  FMon_safety_system_evt(&_m->mon1, safety_system_CANID);
#endif // EVT_USE_DIAG_MONITORS

  return safety_system_CANID;
}

#ifdef EVT_USE_CANSTRUCT

uint32_t Pack_safety_system_evt(safety_system_t* _m, __CoderDbcCanFrame_t__* cframe)
{
  uint8_t i; for (i = 0; (i < safety_system_DLC) && (i < 8); cframe->Data[i++] = 0);

  cframe->Data[0] |= (_m->throttle & (0xFFU));
  cframe->Data[1] |= ((_m->throttle >> 8) & (0xFFU));
  cframe->Data[2] |= (_m->steer & (0xFFU));
  cframe->Data[3] |= ((_m->steer >> 8) & (0xFFU));
  cframe->Data[4] |= ((_m->deadman & (0x01U)) << 4);
  cframe->Data[5] |= (_m->auto_mux & (0x01U)) | ((_m->init & (0x01U)) << 4);
  cframe->Data[6] |= (_m->calib & (0x01U));

  cframe->MsgId = safety_system_CANID;
  cframe->DLC = safety_system_DLC;
  cframe->IDE = safety_system_IDE;
  return safety_system_CANID;
}

#else

uint32_t Pack_safety_system_evt(safety_system_t* _m, uint8_t* _d, uint8_t* _len, uint8_t* _ide)
{
  uint8_t i; for (i = 0; (i < safety_system_DLC) && (i < 8); _d[i++] = 0);

  _d[0] |= (_m->throttle & (0xFFU));
  _d[1] |= ((_m->throttle >> 8) & (0xFFU));
  _d[2] |= (_m->steer & (0xFFU));
  _d[3] |= ((_m->steer >> 8) & (0xFFU));
  _d[4] |= ((_m->deadman & (0x01U)) << 4);
  _d[5] |= (_m->auto_mux & (0x01U)) | ((_m->init & (0x01U)) << 4);
  _d[6] |= (_m->calib & (0x01U));

  *_len = safety_system_DLC;
  *_ide = safety_system_IDE;
  return safety_system_CANID;
}

#endif // EVT_USE_CANSTRUCT


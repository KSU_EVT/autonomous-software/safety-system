#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

// DBC file version
#define VER_EVT_MAJ (0U)
#define VER_EVT_MIN (0U)

// include current dbc-driver compilation config
#include "config.h"

#ifdef EVT_USE_DIAG_MONITORS
// This file must define:
// base monitor struct
// function signature for HASH calculation: (@GetFrameHash)
// function signature for getting system tick value: (@GetSystemTick)
#include "canmonitorutil.h"

#endif // EVT_USE_DIAG_MONITORS


// def @safety_system CAN Message (69   0x45)
#define safety_system_IDE (0U)
#define safety_system_DLC (8U)
#define safety_system_CANID (0x45)

typedef struct
{
#ifdef EVT_USE_BITS_SIGNAL

  int16_t throttle;                          //  [-] Bits=16

  int16_t steer;                             //  [-] Bits=16

  uint8_t deadman : 1;                       //      Bits= 1

  uint8_t auto_mux : 1;                      //      Bits= 1

  uint8_t init : 1;                          //      Bits= 1

  uint8_t calib : 1;                         //      Bits= 1

#else

  int16_t throttle;                          //  [-] Bits=16

  int16_t steer;                             //  [-] Bits=16

  uint8_t deadman;                           //      Bits= 1

  uint8_t auto_mux;                          //      Bits= 1

  uint8_t init;                              //      Bits= 1

  uint8_t calib;                             //      Bits= 1

#endif // EVT_USE_BITS_SIGNAL

#ifdef EVT_USE_DIAG_MONITORS

  FrameMonitor_t mon1;

#endif // EVT_USE_DIAG_MONITORS

} safety_system_t;

// Function signatures

uint32_t Unpack_safety_system_evt(safety_system_t* _m, const uint8_t* _d, uint8_t dlc_);
#ifdef EVT_USE_CANSTRUCT
uint32_t Pack_safety_system_evt(safety_system_t* _m, __CoderDbcCanFrame_t__* cframe);
#else
uint32_t Pack_safety_system_evt(safety_system_t* _m, uint8_t* _d, uint8_t* _len, uint8_t* _ide);
#endif // EVT_USE_CANSTRUCT

#ifdef __cplusplus
}
#endif

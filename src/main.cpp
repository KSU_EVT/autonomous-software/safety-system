#include <Arduino.h>
#include <SBUS.h>
#include <FlexCAN_T4.h>
#include <Metro.h>
#include <safe_sys_can.h>

float channels_[16] = {0.0};
bool failSafe_ = false;
bool lostFrame_ = false;
SBUS x8r(Serial1);

const int safe_sys_id = 24;
const int vcu_id = 12;

int contactor_relay_pin = 21;
int e_brake_relay_pin = 19;

struct SBUS_message {
  float throttle = 0.0;
  float steering = 0.0;
  bool autonomous = false;
  bool deadman = false;
  bool initialize = false;
  bool calibrate = false;
};

struct SBUS_to_CAN {
  int8_t throttle = 0.0;
  int8_t steering = 0.0;
  bool autonomous = false;
  bool deadman = false;
  bool initialize = false;
  bool calibrate = false;
};

Metro can_metro = Metro(10);

SBUS_message rc;
safety_system_t can_message;

static FlexCAN_T4<CAN1, RX_SIZE_256, TX_SIZE_16> CAN;

int count = 0;

static bool can_send_packet(uint32_t id, uint8_t packet[], int32_t len) {
  CAN_message_t msg;
  msg.flags.extended = false;
  msg.id = id;
  msg.len = len;
  memcpy(msg.buf, packet, len);
  Serial.printf("Can Packet Sent\n");
  return (bool)CAN.write(msg);
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  Serial.begin(9600);
  while(!Serial);

  CAN.begin();
  CAN.setBaudRate(250000);

  Serial.println("Starting");

  pinMode (contactor_relay_pin, OUTPUT);
  pinMode (e_brake_relay_pin, OUTPUT);

  x8r.begin();

}



void loop() {
 
if (x8r.readCal(channels_, &failSafe_, &lostFrame_)){
  rc.throttle = channels_[0];
  if (fabs(rc.throttle) < 0.01) rc.throttle = 0.0;
  rc.steering = channels_[1];
  rc.autonomous = channels_[2] > -0.5;
  rc.deadman = channels_[3] > 0.5;
  rc.initialize = channels_[5] > 0.5;
  rc.calibrate = channels_[6] > 0.5;

  //Serial.printf("channels: [0]: %f [1]: %f [2]: %f [3]: %f [4]: %f [5]: %f [6]: %f [7]: %f [8]: %f [9]: %f [10]: %f [11]: %f [12]: %f [13]: %f [14]: %f [15]: %f [16]: %f\n",channels_[0],channels_[1],channels_[2],channels_[3],channels_[4],channels_[5],channels_[6],channels_[7],channels_[8],channels_[9],channels_[10],channels_[11],channels_[12],channels_[13],channels_[14],channels_[15],channels_[16]);

}
 //Serial.printf("==========================\nThrottle: %f\nSteering: %f\nAutonomous State %i\nDeadman State: %i\nInitialize State: %i\nCalibrate State: %i\n=============================\n",rc.throttle,rc.steering,rc.autonomous,rc.deadman,rc.initialize,rc.calibrate);

// Serial.printf("Throttle %f\n", rc.throttle);
// Serial.printf("steering: %f\n", rc.steering);
// Serial.printf("Autonomous State: %i\n",rc.autonomous);
// Serial.printf("Deadman State: %i\n",rc.deadman);
// Serial.printf("Calibrate State: %i\n",rc.calibrate);
// Serial.printf("===========================\n");

if (can_metro.check()){

  uint8_t msg_buf[8];

  // can_msg.throttle = (int8_t) (rc.throttle * 127); //casting floats to signed ints to save space for can packet;
  // can_msg.steering = (int8) (rc.steering * 127);
  // can_msg.autonomous = rc.autonomous;
  // can_msg.deadman = rc.deadman;
  // can_msg.initialize = rc.initialize;
  // can_msg.calibrate = rc.calibrate;

  can_message.throttle = (int16_t) (rc.throttle * 1023);
  can_message.steer = (int16_t) (rc.steering * 1023);
  can_message.auto_mux = rc.autonomous;
  can_message.deadman = rc.deadman;
  can_message.init = rc.initialize;
  can_message.calib = rc.calibrate;

  uint8_t len = sizeof(msg_buf);
  uint8_t ide = 0;

  uint32_t can_id = Pack_safety_system_evt(&can_message, msg_buf, &len, &ide);

  can_send_packet(can_id, msg_buf, len);

}


if (rc.deadman){
  digitalWrite(contactor_relay_pin, HIGH);
  digitalWrite(e_brake_relay_pin, HIGH);
  count ++;

}
else{
  digitalWrite(contactor_relay_pin, LOW);
  digitalWrite(e_brake_relay_pin, LOW);
}

}